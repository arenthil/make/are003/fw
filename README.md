# PL21FGP Firmware

This repository contains the firmware for the Arenthil PL21FGP flight guidance panel. FOR FLIGHT SIMULATION USE ONLY.

This firmware depends on the MobiFlight firmware. It defines various Custom Devices to operate the PL21FGP hardware.

Compatible with the Teensy 4.0 and 4.1 only. This firmware does not support Arduino Mega or other MobiFlight targets. (It would likely not be too difficult to make this work on those platforms, but they haven't been tested. In terms of known changes that would be required, hardware peripherals are hard-coded and I've used some Teensyduino-bundled libraries that would need to be installed separately in platformio.ini or else replaced.)

