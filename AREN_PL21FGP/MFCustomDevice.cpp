#include "MFCustomDevice.h"
#include "device/display.h"
#include "hal/pl21fgp.h"

#include "commandmessenger.h"
#include "allocateMem.h"
#include "MFEEPROM.h"

#include <etl/flat_map.h>
#include <cstdlib>

using aren::pl21fgp::Device;
using aren::pl21fgp::DeviceType;
using aren::pl21fgp::Display;
using aren::pl21fgp::DisplayConfig;
using aren::pl21fgp::getAs1115;

extern MFEEPROM MFeeprom;

/* **********************************************************************************
 * Size of buffer for reading this device's EEPROM data.
 *
 * The custom device pins, type and configuration are stored in the EEPROM. The buffer
 * must fit the longest expected length of any of these three EEPROM strings.
 *
 * The EEPROM addresses for this device are passed to attach() in order to initialise
 * this custom device. They will each be loaded into a buffer of this size and
 * evaluated in order to initialise this instance of the custom device.
 *
 * Example:
 *
 * * type = "MyCustomClass" (15 bytes incl. termination)
 * * configuration = "myConfig" (9 bytes incl. termination)
 * * pins = 6 pins, 2 digits each, delimited by '|' (18 bytes incl. termination)
 *
 * Required buffer size in this scenario is 18.
********************************************************************************** */
static const size_t MEMLEN_STRING_BUFFER = 40;

typedef etl::string<MEMLEN_STRING_BUFFER> MFStringBuf;
typedef std::array<uint8_t, 8> MFPinsBuf;

static const etl::flat_map TYPE_MAP {
    std::pair{ MFStringBuf{"AREN_PL21FGP_DISP"}, DeviceType::DISP }
};

/**
 * Read a period-terminated '.' string from EEPROM.
 *
 * @param addr EEPROM address to start reading from.
 * @param bufsize Size of @c buf
 * @param[out] buf Output buffer. Should be an ETL string type.
 * @return True on success. False on failure, incl. string exceeds buffer length.
 * If false, the contents of buf are undefined (in particular, they may not be
 * null-terminated).
 */
bool MFCustomDevice::readEepromString(uint16_t addr, size_t buf_size, char * buf) {
    size_t ctr = 0;
    char c = MFeeprom.read_byte(addr++);
    while(c != '.') {
        buf[ctr++] = c;
        c = MFeeprom.read_byte(addr++);
        if(ctr >= buf_size) return false;
    }
    buf[ctr++] = '\0';
    return true;
}

template<unsigned size>
bool MFCustomDevice::readEepromString(uint16_t addr, etl::string<size> &out) {
    char buf[size];
    bool ret = readEepromString(addr, size, buf);
    out = buf;
    return ret;
}

/**
 * Read the device type from EEPROM and return the corresponding DeviceType enum value.
 *
 * @param addr_type EEPROM address containing a period-terminated string corresponding to a
 *                  DeviceType. (See the TYPE_MAP constant).
 * @return The matched DeviceType, or DeviceType::UNKNOWN if string does not match any device.
 */
DeviceType MFCustomDevice::getDeviceType(uint16_t addr_type) {
    MFStringBuf buf;
    if(readEepromString(addr_type, buf)) {
        auto res = TYPE_MAP.find(buf);
        if(res != TYPE_MAP.end()) return res->second;
    }
    return DeviceType::UNKNOWN;
}

/**
 * Parse the pins configuration string.
 * @param pins EEPRIM address containing the pins configuration string (period-terminated).
 * @param out Output array.
 * @return Number of pins parsed.
 */
template<unsigned size>
size_t MFCustomDevice::parsePins(uint16_t addr_pins, std::array<uint8_t, size> &out) {
    char buf[MEMLEN_STRING_BUFFER];
    if(readEepromString(addr_pins, MEMLEN_STRING_BUFFER, buf)) {
        size_t counter = 0;
        char * tok = strtok(buf, "|");
        while(tok != nullptr) {
            if(counter >= out.max_size()) {
                cmdMessenger.sendCmd(
                        kStatus,
                        F("MFCustomDevice::attach(): buffer overflow when parsing pins config")
                );
                return counter;  // abort early
            }
            out[counter++] = atoi(tok);
            tok = strtok(nullptr, "|");
        }
        return counter;
    }
    return 0;
}

/**
 * Initialise this custom device instance. This method loads the configurations (pins, type,
 * config) from EEPROM, initialises the appropriate custom device class(es),
 *
 * @param addr_pin EEPROM address for pin configuration.
 * @param addr_type EEPROM address for Custom Device type information.
 * @param addr_config EEPROM address for Custom Device configuration. This is optional configuration
 * that is device-dependent. Delimiter should be '|'; do NOT use any of ".,;".
 */
void MFCustomDevice::attach(uint16_t addr_pin, uint16_t addr_type, uint16_t addr_config)
{
    if (addr_pin == 0 || addr_type == 0 || addr_config == 0) return;

    // read the device type to be initialises
    _type = getDeviceType(addr_type);

    // parse pins
    MFPinsBuf pins;
    uint8_t pins_count = parsePins(addr_pin, pins);

    switch(_type) {
    case DeviceType::DISP:
        _initialized = initDisplayType(addr_config);
        break;
    default:
        _initialized = false;
        cmdMessenger.sendCmd(
                kStatus,
                F("MFCustomDevice::attach(): Device type not supported")
        );
        return;
    }
    _device->begin();
}

bool MFCustomDevice::initDisplayType(uint16_t addr_config) {

    if (!FitInMemory(sizeof(Display))) {
        cmdMessenger.sendCmd(kStatus, F("Cannot initialise PL21FGP::Display: out of memory"));
        return false;
    }

    DisplayConfig cfg;
    char buf[MEMLEN_STRING_BUFFER];
    if(!readEepromString(addr_config, MEMLEN_STRING_BUFFER, buf)) {
        // default if cannot read EEPROM string
        cfg.disp_ms = 3000;
        cfg.global_brightness = 15;
        cfg.brightness.fill(-1);
    }
    else {
        // parse the EEPROM config string

        // disp_ms
        char * tok = strtok(buf, "|");
        char * end;
        int_fast32_t val = strtol(tok, nullptr, 10);
        if(val <= 0) val = 3000; // default, if invalid config
        cfg.disp_ms = constrain(val, 500, INT_FAST32_MAX);

        // global brightness
        tok = strtok(nullptr, "|");
        errno = 0;
        val = strtol(tok, &end, 10);
        if(tok == end || errno) val = 15; // default, if invalid config
        cfg.global_brightness = constrain(val, -1, 15);

        // individual digit brightness
        size_t counter = 0;
        while(tok != nullptr) {
            errno = 0;
            val = strtol(tok, nullptr, 10);
            if(tok == end || errno) val = -1;
            cfg.brightness[counter++] = static_cast<int8_t>(constrain(val, -1, 15));
            tok = strtok(nullptr, "|");
        }
    }

    _device = new (allocateMemory(sizeof(Display)))
            Display(getAs1115(0), getAs1115(1), cfg);
    return true;
}

/**
 * Detach this MobiFlight device. This is called from CustomDevice::Clear() or whenever
 * a new configuration is uploaded from the Connector to the device.
 */
void MFCustomDevice::detach()
{
    _initialized = false;
    _device->end();
}

/**
 * Called every main loop. Run any regular tasks required by this device.
 *
 * The device should avoid running long computations or busy-waits.
 *
 * Called from CustomDevice::update().
 */
void MFCustomDevice::update()
{
    if (!_initialized) return;
    _device->update();
}

/**
 * Called whenever a message is sent to this device from the Connector application.
 *
 * This corresponds to the Outputs configuration in the Connector.
 *
 * Called from CustomDevice::OnSet().
 *
 * @param message_id The ID of the message
 * @param value The value of the message, as a string
 */
void MFCustomDevice::set(int16_t message_id, char * value)
{
    if (!_initialized) return;
    _device->receive(message_id, value);
}
