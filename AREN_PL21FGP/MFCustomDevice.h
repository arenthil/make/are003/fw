#pragma once

#include "device/pl21fgp_device.h"

#include <Arduino.h>
#include <Wire.h>

#include <etl/string.h>
#include <array>

namespace aren::pl21fgp {

//! Arenthil PL21FGP custom device types
enum class DeviceType {
    UNKNOWN = 0,
    DISP = 1
};

} // aren::pl21fgp


// MUST BE OUTSIDE the namespace - for linkage to MobiFlight default firmware
/**
 * @brief PL21FGP Custom Device factory and wrapper.
 *
 * This class presents a standard API to the MobiFlight standard firmware for all Arenthil PL21 FGP-
 * specific devices. It reads the EEPROM configuration for the device and is responsible for
 * instantiating the correct class that corresponds to the device type.
 *
 * Public interface must NOT be modified.
 */
class MFCustomDevice
{
public:
    MFCustomDevice() :
            _initialized(false),
            _type(aren::pl21fgp::DeviceType::UNKNOWN),
            _device(nullptr)
    {}

    void attach(uint16_t adrPin, uint16_t adrType, uint16_t adrConfig);
    void detach();
    void update();
    void set(int16_t messageID, char *setPoint);

private:
    static bool readEepromString(uint16_t addr, size_t buf_size, char * buf);

    template<unsigned size>
    bool readEepromString(uint16_t addr, etl::string<size> &out);

    aren::pl21fgp::DeviceType getDeviceType(uint16_t addr_type);

    template<unsigned size>
    size_t parsePins(uint16_t addr_pins, std::array<uint8_t, size> &out);

    bool initDisplayType(uint16_t adrConfig);

    bool _initialized;
    aren::pl21fgp::DeviceType _type;
    aren::pl21fgp::Device * _device;
};
