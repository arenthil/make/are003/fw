#pragma once

#include <cstdint>

#include <Wire.h>

class TwoWire;

namespace aren::pl21fgp {

/**
 * Driver for the AS1115 I2C 7-segment LED driver and keyscanning IC.
 */
class As1115Driver {
public:
    //! Key channel registers
    enum class KeyReg { A = 0b011100, B = 0b011101};

    //! Digit register segments for non-decoded digits. Value = bit shift.
    enum class Segment {
        DP = 7,
        A  = 6,
        B  = 5,
        C  = 4,
        D  = 3,
        E  = 2,
        F  = 1,
        G  = 0
    };

    enum class DecodeType {
        CodeB = 0,
        Hex = 1
    };

    enum DigitValue {
        ZERO = 0,
        ONE = 1,
        TWO = 2,
        THREE = 3,
        FOUR = 4,
        FIVE = 5,
        SIX = 6,
        SEVEN = 7,
        EIGHT = 8,
        NINE = 9,
        DASH = 10,
        LETTER_E = 11,
        LETTER_H = 12,
        LETTER_L = 13,
        LETTER_P = 14,
        DIGIT_BLANK = 15
    };

    struct Diagnostics {
        uint8_t i2cAddr; //!< I2C address of the AS1115 chip tested
        bool ready : 1; //!< Diagnostics test results ready. If false, remainder of struct is invalid.
        bool error : 1; //!< True if an LED open/short was detected
        bool rset_open : 1; //!< True if current set resistor is open-circuit
        bool rset_short : 1; //!< True if current set resistor is short-circuited
        /**
         * All segments of each digit, bit 7..0 = DP, A, B, C, D, E, F, G.
         * A 1 bit means LED short/open was detected.
         */
        uint8_t digits[8];
    };

    static void enableSelfAddressing(TwoWire &bus);

    explicit As1115Driver(TwoWire &bus, uint8_t i2cAddr);

    void reset();

    void configureDigits(uint8_t mask, uint8_t scanLimit = 0x07);

    void setExternalClock(bool enable=true);
    void setDecoding(DecodeType);
    void setBlink(bool enable=true, bool slow=false, bool sync=false, bool phase_start_on=false);
    void setShutdownMode(bool shutdown, bool reset = false);

    void setDigit(uint8_t index, uint8_t value, bool dp=false);
    void setDigits(uint8_t index, size_t num, const uint8_t * values, uint8_t dp_mask);
    void setIndicator(uint8_t index, Segment segment, bool value);

    void setVisualTestMode(bool enabled);
    void startLedTest();
    Diagnostics getTestResult();

    void setAllIntensity(uint8_t intensity);
    void setIntensity(uint8_t index, uint8_t intensity);

    uint8_t getI2cAddr() const { return _i2cAddrRead >> 1; }
    bool isDecoded(uint8_t index) const;
    uint8_t getDigit(uint8_t index) const;
    bool getIndicator(uint8_t index, Segment) const;
    bool getKey(KeyReg, Segment) const;
    std::array<uint8_t, 2> updateKeys();

private:
    enum class CtlReg {
        DecodeMode   = 0b001001,
        Intensity    = 0b001010,
        ScanLimit    = 0b001011,
        Shutdown     = 0b001100,
        SelfAddr     = 0b101101,
        Feature      = 0b001110,
        DispTestMode = 0b001111,
        Intensity01  = 0b010000,
        Intensity23  = 0b010001,
        Intensity45  = 0b010010,
        Intensity67  = 0b010011
    };

    enum class TestMode {
        VisualTest = 0,
        LedShort = 1,
        LedOpen = 2,

        // read only
        LedTestOngoing = 3,
        LedError = 4,
        RsetOpen = 5,
        RsetShort = 6,
        RESV7 = 7
    };

    enum class Feature {
        ExtClock = 0,
        Reset = 1,
        DecodeSel = 2, // 0 = Code-B, 1 = Hex
        RESV3 = 3,
        BlinkEn = 4,
        BlinkSlow = 5,
        BlinkSync = 6,
        BlinkPhase = 7 // 0 = blink cycle starts with OFF, 1 = starts with ON
    };

    inline static uint8_t _getDigitReg(uint8_t index) { return 0b000001 + index; }
    inline static uint8_t _getDiagnosticReg(uint8_t index) { return 0b010100 + index; }
    inline static uint8_t _getIntensityReg(uint8_t index) { return 0b010000 + index/2; }

    template<typename T> static uint8_t _getBit(T t);

    void _i2cWrite(uint8_t reg, size_t num, uint8_t * buf);
    template<typename RegEnum> void _i2cWrite(RegEnum reg, size_t num, uint8_t * buf);
    template<typename RegEnum> void _i2cWrite(RegEnum reg, uint8_t val);

    void _i2cRead(uint8_t reg, size_t num, uint8_t * out);
    template<typename RegEnum> void _i2cRead(RegEnum reg, size_t num, uint8_t * out);

    TwoWire &_bus;
    const uint8_t _i2cAddrRead, _i2cAddrWrite;
    uint8_t _digitConfig;
    std::array<uint8_t, 8> _digitData;
    uint8_t _keya, _keyb;
    bool _testStarted;
};

template<typename T>
uint8_t As1115Driver::_getBit(T t) {
    return 1 << static_cast<int>(t);
}

template<typename RegEnum>
void As1115Driver::_i2cWrite(RegEnum reg, size_t num, uint8_t * buf) {
    _i2cWrite(static_cast<uint8_t>(reg), num, buf);
}

template<typename RegEnum>
void As1115Driver::_i2cWrite(RegEnum reg, uint8_t val) {
    _i2cWrite(reg, 1, &val);
}

template<typename RegEnum>
void As1115Driver::_i2cRead(RegEnum reg, size_t num, uint8_t * out) {
    _i2cRead(static_cast<uint8_t>(reg), num, out);
}

}