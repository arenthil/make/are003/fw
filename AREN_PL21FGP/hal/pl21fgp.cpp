#include "hal/pl21fgp.h"

#include "hal/as1115_driver.h"
#include "allocateMem.h"

#include <Wire.h>

#include <cstdint>

namespace aren::pl21fgp {

namespace as1115 {
    const size_t NUM = 2;
    struct {
        TwoWire &bus;
        uint8_t addr;
    } cfg[NUM] = {
            {Wire, 0x00},
            {Wire, 0x03}
    };
    As1115Driver * instance[NUM] {nullptr, nullptr};
}


/**
 * Get singleton instance of AS1115 driver.
 * @param index AS1115 index (0 or 1)
 * @return Pointer to instance. Guaranteed non-null for valid indices. Null for
 * invalid indices.
 */
As1115Driver * getAs1115(size_t index) {
    using namespace as1115;
    if(index >= NUM) return nullptr;
    if(instance[index] == nullptr) {
        initI2C();
        instance[index] = new (allocateMemory(sizeof(As1115Driver)))
                          As1115Driver(cfg[index].bus, cfg[index].addr);
    }
    return instance[index];
}

bool _i2c_initialized = false;

//! Initialize the I2C0 bus (Arduino "Wire" object, pins 18/19) for PL21FGP.
void initI2C() {
    if(!_i2c_initialized) {
        Wire.begin();
        Wire.setClock(400000);
        _i2c_initialized = true;
    }
}

}
