#include "as1115_driver.h"

namespace aren::pl21fgp {

/**
 * Enables hard-wired AS1115 I2C addressing.
 *
 * This only needs to be called once per bus, BEFORE init() of AS1115 instances. Since AS1115
 * all have the same address on power-on, this will enable this feature on all of them.
 *
 * @param bus The bus to enable I2C self-addressing on.
 */
void As1115Driver::enableSelfAddressing(TwoWire &bus) {
    bus.beginTransmission(0x00);
    bus.write(static_cast<uint8_t>(CtlReg::SelfAddr));
    bus.write(0x01);
    bus.endTransmission();
}

/**
 *
 * @param bus The bus to use. This bus must already be initialised (i.e. begin() has been called,
 * frequency set to one supported by the chip and processor).
 * @param i2cAddr I2C address of the AS1115 device.
 */
As1115Driver::As1115Driver(TwoWire &bus, uint8_t i2cAddr) :
        _bus(bus), _i2cAddrRead((i2cAddr << 1) | 0x1), _i2cAddrWrite((i2cAddr << 1) | 0x0),
        _digitConfig(0xFF), _digitData({0}), _keya(0), _keyb(0), _testStarted(false)
{
    // noop
}

void As1115Driver::_i2cWrite(uint8_t reg, size_t num, uint8_t * buf) {
    _bus.beginTransmission(_i2cAddrWrite);
    _bus.write(reg);
    while(num > 0) {
        _bus.write(*buf++);
        --num;
    }
    _bus.endTransmission();
}

void As1115Driver::_i2cRead(uint8_t reg, size_t num, uint8_t *out) {
    _bus.beginTransmission(_i2cAddrWrite);
    _bus.write(reg);
    _bus.endTransmission();
    _bus.requestFrom(_i2cAddrRead, num);
    for(size_t i = 0; i < num; i++) out[i] = Wire.read();
}

//! Reset all control registers. Excludes setDigit values.
void As1115Driver::reset() {
    _i2cWrite(CtlReg::Feature, _getBit(Feature::Reset));
}

/**
 * Configure which digit channels are decoded 7-segment digits.
 *
 * @param mask Bitmask. Bit 0 = digit 0, bit 1 = digit 1, etc. For each bit position, 1 indicates
 * a decoded digit, 0 indicates independent control of all 7 LEDs. (DP is independently controlled
 * in either case).
 * @param scanLimit 0 to 7, indicating the highest digit to display, i.e. a value of 3 will show
 * only digits 0 to 3 inclusive. Default 7.
 * @see setDecoding() for choice of Code-B vs. HEX decoding mode.
 * @see isDecoded(): per-digit getter for this configuration.
 */
void As1115Driver::configureDigits(uint8_t mask, uint8_t scanLimit) {
    _digitConfig = mask;
    _i2cWrite(CtlReg::DecodeMode, mask);
    _i2cWrite(CtlReg::ScanLimit, scanLimit);
}

/**
 * Enable or disable the use of an external clock.
 * @param enable
 */
void As1115Driver::setExternalClock(bool enable) {
    uint8_t reg;
    _i2cRead(CtlReg::Feature, 1, &reg);
    if(enable) reg |= _getBit(Feature::ExtClock);
    else       reg &= ~_getBit(Feature::ExtClock);
    _i2cWrite(CtlReg::Feature, reg);
}

/**
 * Set decoding type (Code-B or Hex). Affects how digit values 10-15 are decoded into
 * @param type
 */
void As1115Driver::setDecoding(DecodeType type) {
    uint8_t reg;
    _i2cRead(CtlReg::Feature, 1, &reg);
    if(type == DecodeType::Hex) reg |= _getBit(Feature::DecodeSel);
    else                        reg &= ~_getBit(Feature::DecodeSel);
    _i2cWrite(CtlReg::Feature, reg);
}

/**
 * Set blinking parameters.
 *
 * @param enable
 * @param slow If false, blink period is around 1 second (0.5 on, 0.5 off). If true, 2 seconds.
 * @param sync If true, synchronizes blinking on the rising edge of LD/CS pin. Useful for
 * synchronising the blinking across multiple devices on the same I2C bus.
 * @param phase_start_on If false, the blinking period starts with display off. If true, starts with
 * display on.
 */
void As1115Driver::setBlink(bool enable, bool slow, bool sync, bool phase_start_on) {
    uint8_t reg;
    _i2cRead(CtlReg::Feature, 1, &reg);
    reg &= ~0xF0;
    if(enable) reg |= _getBit(Feature::BlinkEn);
    if(slow)   reg |= _getBit(Feature::BlinkSlow);
    if(sync)   reg |= _getBit(Feature::BlinkSync);
    if(phase_start_on) reg |= _getBit(Feature::BlinkPhase);
    _i2cWrite(CtlReg::Feature, reg);
}

/**
 * Set shutdown mode.
 * @param shutdown Shutdown (200nA typ.) or normal operation.
 * @param reset If true, the Feature register is reset. If using an external clock, must be true.
 */
void As1115Driver::setShutdownMode(bool shutdown, bool reset) {
    uint8_t val = ((shutdown ? 1 : 0) << 7) | (reset ? 1 : 0);
    _i2cWrite(CtlReg::Shutdown, val);
}

/**
 * Set the display value for the digit.
 *
 * @param index Digit index, 0 to 7.
 * @param value Value to show. If the specified digit isDecoded(), this reflects the character to
 * show (i.e. digits 0-9, or 10-15 for other characters depending on the setDecoding() value). If
 * not, this parameter is a bitmask, with bit 7..0 corresponding to segment DP, A, B, ..., G.
 * @param dp If true, decimal point is also illuminated.
 * @see setIndicator() for setting individual LEDs,
 */
void As1115Driver::setDigit(uint8_t index, uint8_t value, bool dp) {
    setDigits(index, 1, &value, dp ? 1 : 0);
}

/**
 * Set the display value for several consecutive digits.
 *
 * This method streams the data for multiple digits without specifying the I2C register address
 * for each digit, which should be ≈2x faster than setDigit() for the 2nd digit onward.
 *
 * @param index Index of first digit to set, 0 to 7.
 * @param num Total number of digits to set.
 * @param values Array of @c num digit values. Each value is as defined for setDigit().
 * @param dp Bitmask for decimal point. Bit 0 = digit 0, etc. Bit 1 = decimal point is on.
 */
void As1115Driver::setDigits(uint8_t index, size_t num, const uint8_t * values, uint8_t dp_mask) {
    if(index > 7 || index + num > 8) return;
    for(size_t i = 0; i < num; ++i) {
        _digitData[index + i] = (values[i] & 0x0f);
        if(dp_mask & 1) _digitData[index + i] |= _getBit(Segment::DP);
        dp_mask >>= 1;
    }
    _i2cWrite(_getDigitReg(index), num, _digitData.data() + index);
}

/**
 * Set a single segment of a digit that is NOT isDecoded().
 * @param index
 * @param segment
 * @param value true = on, false = off.
 */
void As1115Driver::setIndicator(uint8_t index, Segment segment, bool value) {
    if(index > 7 || isDecoded(index)) return;
    if(value) _digitData[index] |= _getBit(segment);
    else      _digitData[index] &= ~_getBit(segment);
    _i2cWrite(_getDigitReg(index), _digitData[index]);
}
/**
 * Start or stop a visual test.
 *
 * This test is to be checked visually; no automated detection is available for this mode.
 * All digits are tested independently from scan limit and shutdown mode.
 * @param enabled
 */
    void As1115Driver::setVisualTestMode(bool enabled) {
        uint8_t value = (enabled ?  _getBit(TestMode::VisualTest) : 0);
        _i2cWrite(CtlReg::DispTestMode, value);
    }

/**
 * Test for LED shorts and open circuits.
 * TODO: way of masking an intentionally disconnected LED?
 */
void As1115Driver::startLedTest() {
    uint8_t value = _getBit(TestMode::LedShort) | _getBit(TestMode::LedOpen);
    _i2cWrite(CtlReg::DispTestMode, value);
    _testStarted = true;
}

As1115Driver::Diagnostics As1115Driver::getTestResult() {
    uint8_t buf;
    _i2cRead(CtlReg::DispTestMode, 1, &buf);

    Diagnostics d {
            .ready = _testStarted && ((buf & _getBit(TestMode::LedTestOngoing)) == 0),
            .error = (buf & _getBit(TestMode::LedError)) != 0,
            .rset_open = (buf & _getBit(TestMode::RsetOpen)) != 0,
            .rset_short = (buf & _getBit(TestMode::RsetShort)) != 0,
            .digits = {0}
    };
    if(d.ready) _i2cRead(_getDiagnosticReg(0), 8, d.digits);
    return d;
}



/**
 * Set intensity of all digits.
 * @param intensity Intensity, 0 to 15, corresponding to 1/16 to 16/16 of the max current.
 */
void As1115Driver::setAllIntensity(uint8_t intensity) {
    intensity &= 0x0F;
    _i2cWrite(CtlReg::Intensity, intensity);
}

/**
 * Set intensity of all digits.
 * @param index Index of the digit, 0 to 7 inclusive
 * @param intensity Intensity, 0 to 15, corresponding to 1/16 to 16/16 of the max current.
 */
void As1115Driver::setIntensity(uint8_t index, uint8_t intensity) {
    if(index > 7) return;

    intensity &= 0x0F;
    uint8_t mask = 0x0F;
    uint8_t regaddr = _getIntensityReg(index);
    uint8_t regval;
    _i2cRead(regaddr, 1, &regval);

    // odd indices are in the upper bits of the register
    if(index % 2 == 1) {
        intensity <<= 4;
        mask <<= 4;
    }

    regval = (regval & ~mask) | intensity;
    _i2cWrite(regaddr, regval);
}

/**
 * @param index Digit index, 0-7.
 * @return True if the digit is configured as a decoded 7-segment digit display, false otherwise.
 * @see configureDigits()
 */
bool As1115Driver::isDecoded(uint8_t index) const {
    return (_digitConfig & (0x1 << index)) != 0;
}

//! Get the current digit value, excluding DP. See setDigit().
uint8_t As1115Driver::getDigit(uint8_t index) const {
    if(index > 7) return 0xFF;
    return _digitData[index] & ~_getBit(Segment::DP);
}

/**
 * Get current LED segment value. Only valid for digits that are not isDecoded(), or the DP segment
 * of isDecoded() digits.
 */
bool As1115Driver::getIndicator(uint8_t index, Segment segment) const {
    if(index > 7) return false;
    return _digitData[index] & _getBit(segment);
}

/**
 * Get the last known state of a key/switch.
 * @param key Keyscan row (A or B)
 * @param segment Segment to check
 * @return true if closed, false if open.
 */
bool As1115Driver::getKey(KeyReg key, Segment segment) const {
    if(key == KeyReg::A) return _keya & _getBit(segment);
    else                 return _keyb & _getBit(segment);
}

/**
 * Update the status of all keys and check for changed keys.
 * @return Array of bitmasks for key register A and B. A '1' bit indicates that the key value has
 * changed since last call to updateKeys() (see getKey() to check value).
 */
std::array<uint8_t, 2> As1115Driver::updateKeys() {
    uint8_t keys[2];
    _i2cRead(KeyReg::A, 2, keys);
    std::array<uint8_t, 2> delta = {
            static_cast<uint8_t>(keys[0] ^ _keya),
            static_cast<uint8_t>(keys[1] ^ _keyb)
    };
    _keya = keys[0];
    _keyb = keys[1];
    return delta;
}

} // namespace
