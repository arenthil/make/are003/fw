#pragma once

#include "device/pl21fgp_device.h"
#include "hal/as1115_driver.h"

#include <elapsedMillis.h>
#include <etl/string.h>
#include <etl/u8string.h>
#include <cstdint>

namespace aren::pl21fgp {

/**
 * MobiFlight configuration for this device:
 * * Type: "AREN_PL21FGP_DISP"
 * * Pins: none
 * * Configuration: example: "3000|-1|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|"
 *      1. disp_ms
 *      2. Global brightness, 0 to 15. Use -1 to set brightness of individual digits.
 *      3-18. Brightness of each digit, 0 to 15. If using global brightness, may be omitted.
 */
struct DisplayConfig {
    static const size_t NUM_DIGITS = 16;
    uint_fast32_t disp_ms; //!< amount of time temporary values (CRS1/2, BARO) are shown
    int8_t global_brightness; //!< global brightness, 0 to 15
    std::array<int8_t, NUM_DIGITS> brightness; //!< Brightness of each digit (0 to 15)
};

class Display : public Device {
public:
    enum MessageId {
        SHUTDOWN = -1,
        POWER_SAVE = -2,
        CRS1 = 0,
        CRS2 = 1,
        BARO_UNIT = 2,
        BARO = 3,
        VS = 4,
        IAS_MACH = 6,
        IAS = 7,
        MACH = 8,
        HDG = 9,
        ALT = 10
    };
    Display(As1115Driver * drv1, As1115Driver * drv2, const DisplayConfig &);
    void begin() override;
    void update() override;
    void end() override;
    void receive(int16_t id, const char * value) override;

    void setAllBlank();
    void showCrs1();
    void showCrs2();
    void showBaro();
    void showVs();

private:
    enum class State { PreInit, Testing, Running, Sleep };
    enum class Disp0 { VS, CRS1, CRS2, BARO };
    static const As1115Driver::Segment SEG_IAS  = As1115Driver::Segment::E,
                                       SEG_MACH = As1115Driver::Segment::F;

    void transitionState();
    void updateTesting();
    void updateRunning();

    static etl::string<16> buildErrorSegmentString(uint8_t digit_mask);
    template<typename Integer, size_t num>
    static void convertToDigits(Integer val, size_t digits, etl::u8string<num> &out, bool leading_zeros=false);

    static const size_t NUM_DRV = 2;
    const std::array<As1115Driver*, NUM_DRV> _drivers;
    DisplayConfig _cfg;
    State _state, _next_state;
    Disp0 _cur_disp0;
    uint16_t _crs1, _crs2, _baro, _ias, _mach, _hdg, _alt;
    int16_t _vs;
    bool _baro_mbar, _ias_mach;

    // timing things
    elapsedMillis _update_timer;
    static const uint_fast32_t UPDATE_PERIOD_MS = 16;
    elapsedMillis _disp0_timer;
};

}
