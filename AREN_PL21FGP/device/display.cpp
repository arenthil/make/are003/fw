#include "display.h"

#include "commandmessenger.h"
#include <Arduino.h>
#include <cstdio>

namespace aren::pl21fgp {

#define TAG "PL21FGP::Display::"

static const uint32_t MBAR_PER_INHG_X100 = 3386;
static const uint32_t ROUND_MBAR_TO_INHG = 17; // rounding term to 0.01inHg -> 17/3386 = +0.005
static const uint32_t ROUND_INHG_TO_MBAR = 5000; // rounding term to 1 mBar -> 5000/10000 = +0.5

Display::Display(As1115Driver * left, As1115Driver * right, const DisplayConfig &cfg) :
        _drivers({left, right}), _cfg(cfg),
        _state(State::PreInit), _next_state(State::PreInit),
        _cur_disp0(Disp0::VS),
        _crs1(0), _crs2(0), _baro(0), _ias(0), _mach(0), _hdg(0), _alt(0), _vs(0),
        _baro_mbar(false), _ias_mach(false),
        _update_timer(0)
{
    // noop
}

void Display::begin() {
    // basic configuration
    _drivers[0]->configureDigits(0b01111111); // 0-3 VS, 4-6 SPEED, 7 indicators
    _drivers[1]->configureDigits(0b00111111); // 0-2 HDG, 3-5 ALT, 6-7 indicators
    for(auto driver : _drivers) {
        driver->setExternalClock(false);
        driver->setDecoding(As1115Driver::DecodeType::CodeB);
        driver->setBlink(false);
    }
    setAllBlank();

    // brightness
    if(_cfg.global_brightness > 0) {
        for(auto driver: _drivers) {
            driver->setAllIntensity(constrain(_cfg.global_brightness, 0, 15));
        }
    }
    for(size_t i = 0; i < _cfg.brightness.size(); i++) {
        if(_cfg.brightness[i] < 0) continue;
        _drivers[i/8]->setIntensity(i%8, constrain(_cfg.brightness[i], 0, 15));
    }

    // enable driver and perform open/short test
    for(auto driver : _drivers) driver->setShutdownMode(false);
    _next_state = State::Testing;
}

void Display::update() {
    if(_state != _next_state) transitionState();
    switch(_state) {
    case State::PreInit:
    case State::Sleep:
        break;
    case State::Testing:
        updateTesting();
        break;
    case State::Running:
        updateRunning();
        break;
    }
}

void Display::transitionState() {
    switch(_next_state) {
    case State::PreInit:
        for(auto driver: _drivers) driver->setShutdownMode(true);
        break;
    case State::Testing:
        for(auto driver: _drivers) driver->startLedTest();
        break;
    case State::Running:
        _update_timer = UPDATE_PERIOD_MS; // set the next update to run immediately
        showVs();
        break;
    case State::Sleep:
        for(auto driver: _drivers) driver->setShutdownMode(true);
        break;
    default: {
        char buf[64] = "";
        snprintf(buf, 64, TAG "enterState(): unknown state %i", static_cast<int>(_next_state));
        cmdMessenger.sendCmd(kStatus, buf);
        _next_state = _state; // cancel the transition
        return;
    }
    }
    _state = _next_state;
}

void Display::updateTesting() {
    std::array<As1115Driver::Diagnostics, NUM_DRV> diags;
    for(size_t i = 0; i < _drivers.size(); ++i) {
        diags[i] = _drivers[i]->getTestResult();
        if(!diags[i].ready) return; // test not finished
    }

    // if all tests finished
    for(auto diag: diags) {
        if(diag.rset_open || diag.rset_short) {
            char buf[64] = "";
            snprintf(buf, 64, TAG "0x%x SELF TEST FAILED: RSET FAULT", diag.i2cAddr);
            cmdMessenger.sendCmd(kStatus, buf);
        }
        if(diag.error) {
            for(size_t i = 0; i < 8; ++i) {
                if(diag.digits[i] == 0) continue;
                char buf[64] = "";
                etl::string<32> segs = buildErrorSegmentString(diag.digits[i]);
                snprintf(buf, 64, "0x%x SELF TEST FAILED: LED FAULT DIG%i %s",
                         diag.i2cAddr, i, segs.data());
                cmdMessenger.sendCmd(kStatus, buf);
            }
        }
    }

    _next_state = State::Running; // device can still run even if self-test failed
}

etl::string<16> Display::buildErrorSegmentString(uint8_t digit_mask) {
    etl::string<16> buf = "SEG ";
    char seg = 'A';

    if(digit_mask & 0x80) buf += "DP "; // check top bit
    digit_mask <<= 1;
    while(digit_mask != 0) {
        if(digit_mask & 0x80) buf += seg;
        digit_mask <<= 1;
        seg += 1;
    }
    return buf;
}

template<typename Integer, size_t num>
void Display::convertToDigits(Integer val, size_t digits, etl::u8string<num> &out, bool leading_zeros) {
    auto start = out.data_end();
    // preallocate space, since the conversion goes from least to most significant digit
    if(!leading_zeros) out.append(digits, As1115Driver::DigitValue::DIGIT_BLANK);
    else               out.append(digits, As1115Driver::DigitValue::ZERO);
    auto cur = out.data_end();
    // negative sign
    if(val < 0) *start++ = As1115Driver::DigitValue::DASH;

    // convert the value one digit at a time
    while(val != 0 && cur > start) {
        auto q = ldiv(val, 10);
        val = q.quot;
        *--cur = q.rem;
    }
}

void Display::updateRunning() {
    if(_update_timer >= UPDATE_PERIOD_MS) {
        _update_timer = 0;

        //
        // Process driver #0 values
        //

        // first value: check timer - reset to VS if time expired
        if(_disp0_timer >= _cfg.disp_ms) showVs();

        etl::u8string<8> digits;
        uint8_t dp = 0;

        // First value: calculate digits
        switch(_cur_disp0) {
        case Disp0::VS:
            // TODO: what if negative? not enough digits
            convertToDigits(_vs, 4, digits);
            break;
        case Disp0::BARO:
            convertToDigits(_baro, 4, digits);
            // decimal point for hPa vs. inHg
            if(_baro_mbar) dp |= (1 << 3);
            else           dp |= (1 << 1);
            break;
        case Disp0::CRS1:
            convertToDigits(_crs1, 4, digits);
            break;
        case Disp0::CRS2:
            convertToDigits(_crs2, 4, digits);
            break;
        }

        // Speed (IAS/MACH)
        if(_ias_mach) {
            convertToDigits(_mach, 3, digits, true);
            dp |= (1 << 4);
        }
        else {
            convertToDigits(_ias, 3, digits);
            dp |= (1 << 6);
        }

        // Send driver #0 values
        _drivers[0]->setDigits(0, 7, digits.data(), dp);
        _drivers[0]->setIndicator(7, SEG_IAS, !_baro_mbar);
        _drivers[0]->setIndicator(7, SEG_MACH, _baro_mbar);

        //
        // Prepare driver #1 values
        //
        digits.clear();
        dp = 0;

        // Heading
        convertToDigits(_hdg, 3, digits);
        dp |= (1 << 2);

        // Altitude
        convertToDigits(_alt, 3, digits);
        dp |= (1 << 5);

        // Send driver #1 values
        _drivers[1]->setDigits(0, 6, digits.data(), dp);
    }
}

void Display::end() {
    _next_state = State::PreInit;
    transitionState();
}

void Display::receive(int16_t message_id, const char * value) {
    float fVal;
    switch(message_id) {
    case SHUTDOWN:
        _next_state = State::PreInit;
        break;
    case POWER_SAVE:
        if(*value == '0')
            _next_state = State::Running;
        else
            _next_state = State::Sleep;
        break;
    case CRS1:
        fVal = strtof(value, nullptr);
        if((fVal != 0.0f || strcmp(value, "0") == 0) && fVal <= 360.0f)
            _crs1 = lroundf(fVal);
        break;
    case CRS2:
        fVal = strtof(value, nullptr);
        if((fVal != 0.0f || strcmp(value, "0") == 0) && fVal <= 360.0f)
            _crs2 = lroundf(fVal);
        break;
    case BARO_UNIT: {
        bool _old_baro_mbar = _baro_mbar;
        _baro_mbar = (strcmp(value, "0") != 0);
        if(_old_baro_mbar != _baro_mbar) {
            if(_baro_mbar)  // inHg -> mBar
                _baro = (static_cast<uint32_t>(_baro) * MBAR_PER_INHG_X100 + ROUND_INHG_TO_MBAR)
                        / 100UL / 100UL;
            else  // mBar -> inHg
                _baro = (static_cast<uint32_t>(_baro) * 100UL + ROUND_MBAR_TO_INHG)
                        / MBAR_PER_INHG_X100;
        }
        break;
    }
    case BARO:
        fVal = strtof(value, nullptr);
        if(_baro_mbar)
            _baro = constrain(lroundf(fVal), 0L, 9999L);
        else
            _baro = constrain(lroundf(100 * fVal), 0L, 9999L);
        break;
    case VS:
        fVal = strtof(value, nullptr);
        _vs = constrain(lroundf(fVal), -9999L, 9999L);
        break;
    case IAS_MACH:
        _ias_mach = (strcmp(value, "0") != 0);
        break;
    case IAS:
        fVal = strtof(value, nullptr);
        _ias = constrain(lroundf(fVal), 0L, 999L);
        break;
    case MACH:
        fVal = strtof(value, nullptr);
        _mach = constrain(lroundf(100 * fVal), 0L, 999L);
        break;
    case HDG:
        fVal = strtof(value, nullptr);
        if((fVal != 0.0f || strcmp(value, "0") == 0) && fVal <= 360.0f)
            _hdg = lroundf(fVal);
        break;
    case ALT:
        fVal = strtof(value, nullptr);
        _alt = constrain(lroundf(fVal/100.0f), -99L, 999L);
        break;
    default: {
        char buf[64] = "";
        snprintf(buf, 64, TAG "receive(): Unknown message ID %i", message_id);
        cmdMessenger.sendCmd(kStatus, buf);
    }
    }
}

void Display::setAllBlank() {
    std::array<uint8_t, 8> digits;
    digits.fill(As1115Driver::DigitValue::DIGIT_BLANK);
    for(auto driver: _drivers) {
        driver->setDigits(0, 8, digits.data(), 0);
    }
}

void Display::showVs() {
    _cur_disp0 = Disp0::VS;
    _disp0_timer = 0;
}

void Display::showCrs1() {
    _cur_disp0 = Disp0::CRS1;
    _disp0_timer = 0;
}

void Display::showCrs2() {
    _cur_disp0 = Disp0::CRS2;
    _disp0_timer = 0;
}

void Display::showBaro() {
    _cur_disp0 = Disp0::BARO;
    _disp0_timer = 0;
}

}
