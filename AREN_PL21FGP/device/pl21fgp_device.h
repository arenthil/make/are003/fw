#pragma once

#include <cstdint>

namespace aren::pl21fgp {

/**
 * Abstract base class for MobiFlight Custom Device concrete classes. This class is instantiated
 * and called by the MFCustomDevice factory/wrapper.
 *
 * In general, any parameters an implementation requires should be passed in the constructor.
 */
class Device {
public:
    //! Initialise this device's state.
    virtual void begin() = 0;

    /**
     *  Called every main loop. Run any regular tasks required by this device.
     *
     * This method should avoid running long computations or busy-waits.
     */
    virtual void update() = 0;

    //! Uninitialize this device.
    virtual void end() = 0;

    /**
     * Called whenever a message is sent to this device from the Connector application.
     *
     * This corresponds to the Outputs configuration in the Connector.
     */
    virtual void receive(int16_t id, const char * value) = 0;
};

} // aren::pl21fgp
// aren
